import Client from '../../dev/http-client';
import type { CatMinInfo, CatsList } from '../../dev/types';

const cats: CatMinInfo[] = [{ name: 'ТестерКристина', description: '', gender: 'male' }];

let catId;

const FakeId = 'аовтт';
const catDescription = 'Описание хорошего котика';


const HttpClient = Client.getInstance();

describe('API', () => {
  beforeAll(async () => {
    try {
      const add_cat_response = await HttpClient.post('core/cats/add', {
        responseType: 'json',
        json: { cats },
      });
      if ((add_cat_response.body as CatsList).cats[0].id) {
        catId = (add_cat_response.body as CatsList).cats[0].id;
      } else throw new Error('Не получилось получить id тестового котика!');
          } catch (error) { throw new Error('Не удалось создать котика для автотестов!');
    }
  });

  afterAll(async () => {
    await HttpClient.delete(`core/cats/${catId}/remove`, {
      responseType: 'json',
    });
  });

  it('Найти существующего котика по id', async () => {
    const response = await HttpClient.get(`core/cats/get-by-id?id=${catId}`, {
      responseType: 'json',
    });
    expect(response.statusCode).toEqual(200);

    expect(response.body as any).toEqual
        const Cat = {
        id: catId,
        ...cats[0], 
        likes: 0,
        dislikes: 0,
        tags: null
    };

});

it('Попытка найти котика по некорректному id вызовет ошибку', async () => {
  await expect(
    HttpClient.get(`core/cats/get-by-id?id=${FakeId}`, {
      responseType: 'json',
    })
  ).rejects.toThrowError('Response code 400 (Bad Request)');
});

it('Добавить описание котику', async () => {
  const response = await HttpClient.post(`core/cats/save-description`, {
    responseType: 'json',
    json: {
      "catId": catId,
      "catDescription": catDescription
    },
  });
  expect(response.statusCode).toEqual(200);

  expect(response.body).toEqual
  const Cat = ({
    id: catId,
    name: cats[0].name,
    description: catDescription,
    tags: null,
    gender: cats[0].gender,
    likes: expect.any(Number),
    dislikes: expect.any(Number)
})


});
});


it('Получение списка котов сгруппированных по группам', async () => {
  const response = await HttpClient.get('core/cats/allByLetter', {
    responseType: 'json',
  });
  expect(response.statusCode).toEqual(200);

  expect(response.body).toEqual({
    count_all: expect.any(Number),
    count_output: expect.any(Number),
    groups: expect.arrayContaining([
        expect.objectContaining({
            cats: expect.arrayContaining([
                expect.objectContaining({
                    count_by_letter: expect.any(String),
                    description: expect.any(String),
                    dislikes: expect.any(Number),
                    gender: expect.any(String),
                    id: expect.any(Number),
                    likes: expect.any(Number),
                    name: expect.any(String),
                    tags: null
                })
            ]),
            count_by_letter: expect.any(Number),
            count_in_group: expect.any(Number),
            title: expect.any(String)
        })
    ])
});
});


